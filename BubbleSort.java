import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;


public class BubbleSort{
	static int enumerate=0;
	static Object [][] array = new Object[11][5];
	static int inputCode;
	static String inputTypes;
	static String inputColor;
	static int inputQty;
	static int Cost;
	static Scanner input = new Scanner(System.in);

	
	public static void Menu(){
		System.out.println("\n\nMenu : ");
		System.out.println("1. Input items");
		System.out.println("2. Sort by...");
		System.out.println("0. Exit");
	}
	
	public static void SortBy(){
		System.out.println("\n\nSort by : ");
		System.out.println("1. Code");
		System.out.println("2. Types Goods");
		System.out.println("3. Color");
		System.out.println("4. Quantity");
		System.out.println("5. Cost");
		System.out.println("0. Exit");
	}
	
	public static void main(String[]args) throws IOException{
		boolean end;
		PrintWriter write   = new PrintWriter("Data.txt");
		PrintWriter sorting = new PrintWriter("SortedData.txt");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String inputData = null;
		int choice = 0;
		do{
			Menu();
			System.out.print("\nSubmit your choice : ");
			inputData = bufferedReader.readLine();
					switch(inputData){
						case "1":
						do{
							end = true;
							do{
								System.out.print("Code        : ");
								
								try{
									inputCode = input.nextInt();
									break;
								}catch(InputMismatchException e){
									System.out.println("Error! Insert code using number");
									input.nextLine();
									continue;
								}
							}while(true);
							
							for(int i=0; i<enumerate; i++){
								if(inputCode==(int)array[i][0]){
								end=false;
								System.out.println("Code has already used");
								}
							}
							
							
						}while (end == false);
						input.nextLine();
						 
						 
						do{
							end = true;
							System.out.print("Type Goods : ");
							String inputTypes = null;
							try{
								inputTypes = bufferedReader.readLine();
							}catch(IOException error){
								System.out.println("Error Input "+error.getMessage());
							}
							for(int i=0; i<enumerate; i++){
								if(inputTypes.contentEquals((String) array[i][1])){
									end = false;
									System.out.println("Error! Type Goods has been used");
								}
							}
						}
						while (end == false);
						
						do{
							end = true;
							System.out.print("Color       : ");
							String inputColor = null;
							try{
								inputColor = bufferedReader.readLine();
							}catch(IOException error){
								System.out.println("Error Input "+error.getMessage());
							}
						}
						while (end == false);
						
						do{
							System.out.print("Quantity    : ");
							String inputQty = null;
							try{
								inputQty = bufferedReader.readLine();
								int Quantity = Integer.parseInt(inputQty);
							}catch(InputMismatchException e){
								System.out.println("Insert code using number");
							}
						}
						while (end == false);
						
						do{
							System.out.print("Cost        : Rp. ");
							String inputCost = null;
							int Cost = Integer.parseInt(inputCost);
							try{
								inputCost = bufferedReader.readLine();
							}catch(IOException error){
								System.out.println("Error Input "+error.getMessage());
							}
						}while (true);
						
							array[enumerate][0] = inputCode;
							array[enumerate][1] = inputTypes;
							array[enumerate][2] = inputColor;
							array[enumerate][3] = inputQty;
							array[enumerate][4] = Cost;
						
							write.println(inputCode);
							write.println(inputTypes);
							write.println(inputColor);
							write.println(inputQty);
							write.println(Cost);
							write.println("====");
						
						
							enumerate++;
						input.nextLine();
						break;
						
						
						case "2":
						SortBy();
						System.out.print("\nSubmit your choice : ");
						inputData = bufferedReader.readLine();
						switch (inputData){
							case "1":	
								String sort = "";
								int intsort = -1;
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										if( ( (int) array [i][0] ) < ( (int) array [j][0] )){
											
											intsort     = (int) array[j][0];
											array[j][0] = array[i][0];
											array[i][0] = intsort;
											
											sort = (String) array[j][1];
											array[j][1]  = array[i][1];
											array [i][1] = sort;
											
											sort         = (String) array[j][2];
											array[j][2]  = array[i][2];
											array [i][2] = sort;
											
											intsort     = (int) array[j][3];
											array[j][3] = array[i][3];
											array[i][3] = intsort;
											
											intsort     = (int) array[j][4];
											array[j][4] = array[i][4];
											array[i][4] = intsort;
										}
									}
								}
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										sorting.println(array[i][j]);
									}
									sorting.println("====");
								}
								sorting.close();
								System.out.println("File 'SortedData.txt' has been created ");
								break;
							case "2":
								sort="";
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										if(((String) array[i][1]).compareToIgnoreCase((String) array[j][1]) < 0){	
											
											intsort = (int) array [j][0];
											array[j][0] = array [i][0];
											array[i][0] = intsort;
											
											sort = (String) array[j][1];
											array[j][1]  = array[i][1];
											array [i][1] = sort;
											
											sort         = (String) array[j][2];
											array[j][2]  = array[i][2];
											array [i][2] = sort;
											
											intsort     = (int) array[j][3];
											array[j][3] = array[i][3];
											array[i][3] = intsort;
											
											intsort     = (int) array[j][4];
											array[j][4] = array[i][4];
											array[i][4] = intsort;
										}
									}
								}
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										sorting.println(array[i][j]);
									}
									sorting.println("====");
								}
								sorting.close();
								System.out.println("File 'SortedData.txt' has been created ");
								break;
							
							case "3":
								sort="";
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										if ( ( (String) array[i][1]).compareToIgnoreCase ((String) array[j][1]) <0 ){
											intsort     = (int) array [j][0];
											array[j][0] = array [i][0];
											array[i][0] = intsort;
											
											sort         = (String) array[j][1];
											array[j][1]  = array[i][1];
											array [i][1] = sort;
											
											sort         = (String) array[j][2];
											array[j][2]  = array[i][2];
											array [i][2] = sort;
											
											intsort     = (int) array[j][3];
											array[j][3] = array[i][3];
											array[i][3] = intsort;
											
											intsort     = (int) array[j][4];
											array[j][4] = array[i][4];
											array[i][4] = intsort;
										}
									}
								}
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										sorting.println(array[i][j]);
									}
									sorting.println("====");
								}
								sorting.close();
								System.out.println("File 'SortedData.txt' has been created ");
								break;
							
							case "4":
								sort="";
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										if ( ( (String) array[i][1]).compareToIgnoreCase ((String) array[j][1]) <0 ){
											intsort     = (int) array [j][0];
											array[j][0] = array [i][0];
											array[i][0] = intsort;
											
											sort         = (String) array[j][1];
											array[j][1]  = array[i][1];
											array [i][1] = sort;
											
											sort         = (String) array[j][2];
											array[j][2]  = array[i][2];
											array [i][2] = sort;
											
											intsort     = (int) array[j][3];
											array[j][3] = array[i][3];
											array[i][3] = intsort;
											
											intsort     = (int) array[j][4];
											array[j][4] = array[i][4];
											array[i][4] = intsort;
										}
									}
								}
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										sorting.println(array[i][j]);
									}
									sorting.println("====");
								}
								sorting.close();
								System.out.println("File 'SortedData.txt' has been created ");
								break;
						
							case "5":
								sort="";
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										if ( ( (String) array[i][1]).compareToIgnoreCase ((String) array[j][1]) <0 ){
											intsort     = (int) array [j][0];
											array[j][0] = array [i][0];
											array[i][0] = intsort;
											
											sort         = (String) array[j][1];
											array[j][1]  = array[i][1];
											array [i][1] = sort;
											
											sort         = (String) array[j][2];
											array[j][2]  = array[i][2];
											array [i][2] = sort;
											
											intsort     = (int) array[j][3];
											array[j][3] = array[i][3];
											array[i][3] = intsort;
											
											intsort     = (int) array[j][4];
											array[j][4] = array[i][4];
											array[i][4] = intsort;
										}
									}
								}
								for(int i=0; i<enumerate; i++){
									for(int j=0; j<enumerate; j++){
										sorting.println(array[i][j]);
									}
									sorting.println("====");
								}
								sorting.close();
								System.out.println("File 'SortedData.txt' has been created ");
								break;
							default:
							
							break;
						}
						
						sorting.close();
						System.out.println("File 'SortedData.txt' has been created ");
						break;
					
					default:
						System.out.println("Input Correctly");
						break;
					}
					
		}while(inputData.contentEquals("2")==false);
		write.close();
		sorting.close();
	}
		}
	